/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ericquan8.trojanserver;

/**
 *
 * @author eric
 */
public class Key {
    
    public static final int delayTime = 500;
    public static final String FILE = "file";
    public static final String COMMAND = "command";
    public static final String COMMAND_NO_REPLY = "command_no_reply";
    public static final String SCREEN = "screen";
    public static final String BROSWER = "broswer";
    public static final String KILL = "kill";
    
    //服务器启动端口
    public static int TROJAN_SERVER_PORT;
    // 服务器接受客户端图片的端口
    public static int MONITOR_SERVER_PORT;
    public static final int SINGLE_MONITOR_DELAY_TIME = 50;
    public static final String OPERATION = "operation";

}
